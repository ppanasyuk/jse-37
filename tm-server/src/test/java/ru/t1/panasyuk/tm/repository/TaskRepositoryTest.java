package ru.t1.panasyuk.tm.repository;

import io.qameta.allure.junit4.DisplayName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.panasyuk.tm.api.repository.ITaskRepository;
import ru.t1.panasyuk.tm.api.service.IConnectionService;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.model.Task;
import ru.t1.panasyuk.tm.service.ConnectionService;
import ru.t1.panasyuk.tm.service.PropertyService;
import ru.t1.panasyuk.tm.util.SystemUtil;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@DisplayName("Тестирование репозитория задач")
public class TaskRepositoryTest {

    private final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private final String USER_1_ID = SystemUtil.generateGuid();

    @NotNull
    private final String USER_2_ID = SystemUtil.generateGuid();

    @NotNull
    private final String PROJECT_1_ID = SystemUtil.generateGuid();

    @NotNull
    private final String PROJECT_2_ID = SystemUtil.generateGuid();

    @NotNull
    private List<Task> taskList;

    @NotNull
    private ITaskRepository taskRepository;

    @NotNull
    private static IConnectionService connectionService;

    @NotNull
    private Connection connection;

    @BeforeClass
    public static void initConnection() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initRepository() throws Exception {
        connection = connectionService.getConnection();
        taskList = new ArrayList<>();
        taskRepository = new TaskRepository(connection);
        try {
            for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
                @NotNull final Task task = new Task();
                task.setName("Task " + i);
                task.setDescription("Description " + i);
                if (i < 5) {
                    task.setUserId(USER_1_ID);
                    task.setProjectId(PROJECT_1_ID);
                } else {
                    task.setUserId(USER_2_ID);
                    task.setProjectId(PROJECT_2_ID);
                }
                taskList.add(task);
                taskRepository.add(task);
            }
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @After
    public void closeConnection() throws Exception {
        taskRepository.clear(USER_1_ID);
        taskRepository.clear(USER_2_ID);
        connection.commit();
        connection.close();
    }

    @Test
    @DisplayName("Добавление задачи")
    public void addTest() throws Exception {
        int expectedNumberOfEntries = taskRepository.getSize() + 1;
        @NotNull final String taskName = "Task Name";
        @NotNull final String taskDescription = "Task Description";
        @NotNull final Task task = new Task();
        task.setName(taskName);
        task.setDescription(taskDescription);
        taskRepository.add(USER_1_ID, task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
        @Nullable final Task createdTask = taskRepository.findOneById(task.getId());
        Assert.assertNotNull(createdTask);
        Assert.assertEquals(USER_1_ID, createdTask.getUserId());
        Assert.assertEquals(taskName, createdTask.getName());
        Assert.assertEquals(taskDescription, createdTask.getDescription());
    }

    @Test
    @DisplayName("Добавление Null задачи")
    public void addNullTest() throws Exception {
        @Nullable final Task createdTask = taskRepository.add(USER_1_ID, null);
        Assert.assertNull(createdTask);
    }

    @Test
    @DisplayName("Добавление списка задач")
    public void addAllTest() throws Exception {
        int expectedNumberOfEntries = taskRepository.getSize() + 2;
        @NotNull final List<Task> tasks = new ArrayList<>();
        @NotNull final String firstTaskName = "First Task Name";
        @NotNull final String firstTaskDescription = "Task Description";
        @NotNull final Task firstTask = new Task();
        firstTask.setName(firstTaskName);
        firstTask.setDescription(firstTaskDescription);
        tasks.add(firstTask);
        @NotNull final String secondTaskName = "Second Task Name";
        @NotNull final String secondTaskDescription = "Task Description";
        @NotNull final Task secondTask = new Task();
        secondTask.setName(secondTaskName);
        secondTask.setDescription(secondTaskDescription);
        tasks.add(secondTask);
        @NotNull final Collection<Task> addedTasks = taskRepository.add(tasks);
        Assert.assertTrue(addedTasks.size() > 0);
        int actualNumberOfEntries = taskRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    @DisplayName("Удалить все задачи для пользователя")
    public void clearForUserTest() throws Exception {
        int expectedNumberOfEntries = 0;
        taskRepository.clear(USER_1_ID);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize(USER_1_ID));
    }

    @Test
    @DisplayName("Проверка существования задачи по Id")
    public void existByIdTrueTest() throws Exception {
        boolean expectedResult = true;
        @Nullable final Task task = taskRepository.findOneByIndex(1);
        Assert.assertNotNull(task);
        @Nullable final String taskId = task.getId();
        Assert.assertEquals(expectedResult, taskRepository.existsById(taskId));
    }

    @Test
    @DisplayName("Проверка несуществования задачи по Id")
    public void existByIdFalseTest() throws Exception {
        boolean expectedResult = false;
        Assert.assertEquals(expectedResult, taskRepository.existsById("qwerty"));
    }

    @Test
    @DisplayName("Проверка существования задачи по Id для пользователя")
    public void existByIdForUserTrueTest() throws Exception {
        boolean expectedResult = true;
        @Nullable final Task task = taskRepository.findOneByIndex(USER_1_ID, 1);
        Assert.assertNotNull(task);
        @Nullable final String taskId = task.getId();
        Assert.assertEquals(expectedResult, taskRepository.existsById(USER_1_ID, taskId));
    }

    @Test
    @DisplayName("Проверка несуществования задачи по Id для пользователя")
    public void existByIdForUserFalseTest() throws Exception {
        boolean expectedResult = false;
        Assert.assertEquals(expectedResult, taskRepository.existsById(USER_1_ID, "qwerty"));
    }

    @Test
    @DisplayName("Поиск всех задач")
    public void findAllTest() throws Exception {
        @NotNull final List<Task> tasks = taskRepository.findAll();
        Assert.assertTrue(tasks.size() > 0);
    }

    @Test
    @DisplayName("Поиск всех задач по Id проекта")
    public void findAllByProjectIdTest() throws Exception {
        @NotNull List<Task> taskListForProject = taskList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .filter(m -> PROJECT_1_ID.equals(m.getProjectId()))
                .collect(Collectors.toList());
        @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(USER_1_ID, PROJECT_1_ID);
        Assert.assertEquals(taskListForProject, tasks);
    }

    @Test
    @DisplayName("Поиск всех задач по компаратору")
    public void findAllWithComparatorTest() throws Exception {
        @NotNull Comparator<Task> comparator = Sort.BY_NAME.getComparator();
        int allProjectsSize = taskRepository.findAll().size();
        @NotNull List<Task> tasks = taskRepository.findAll(comparator);
        Assert.assertEquals(allProjectsSize, tasks.size());
        comparator = Sort.BY_CREATED.getComparator();
        tasks = taskRepository.findAll(comparator);
        Assert.assertEquals(allProjectsSize, tasks.size());
        comparator = Sort.BY_STATUS.getComparator();
        tasks = taskRepository.findAll(comparator);
        Assert.assertEquals(allProjectsSize, tasks.size());
    }

    @Test
    @DisplayName("Поиск всех задач для пользователя")
    public void findAllForUserTest() throws Exception {
        @NotNull List<Task> taskListForUser = taskList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull final List<Task> tasks = taskRepository.findAll(USER_1_ID);
        Assert.assertEquals(taskListForUser.size(), tasks.size());
    }

    @Test
    @DisplayName("Поиск всех задач для пользователя по компаратору")
    public void findAllWithComparatorForUser() throws Exception {
        @NotNull List<Task> taskListForUser = taskList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .filter(m -> PROJECT_1_ID.equals(m.getProjectId()))
                .collect(Collectors.toList());
        @NotNull Comparator<Task> comparator = Sort.BY_NAME.getComparator();
        @NotNull List<Task> tasks = taskRepository.findAll(USER_1_ID, comparator);
        Assert.assertEquals(taskListForUser, tasks);
        comparator = Sort.BY_CREATED.getComparator();
        tasks = taskRepository.findAll(USER_1_ID, comparator);
        Assert.assertEquals(taskListForUser, tasks);
        comparator = Sort.BY_STATUS.getComparator();
        tasks = taskRepository.findAll(USER_1_ID, comparator);
        Assert.assertEquals(taskListForUser, tasks);
    }

    @Test
    @DisplayName("Поиск задачи по Id")
    public void findOneByIdTest() throws Exception {
        @Nullable Task task;
        for (int i = 0; i < taskList.size(); i++) {
            task = taskList.get(i);
            Assert.assertNotNull(task);
            @NotNull final String taskId = task.getId();
            @Nullable final Task foundTask = taskRepository.findOneById(taskId);
            Assert.assertNotNull(foundTask);
        }
    }

    @Test
    @DisplayName("Поиск задачи по Id равному Null")
    public void findOneByIdNullTest() throws Exception {
        @Nullable final Task foundTask = taskRepository.findOneById("qwerty");
        Assert.assertNull(foundTask);
        @Nullable final Task foundTaskNull = taskRepository.findOneById(null);
        Assert.assertNull(foundTaskNull);
    }

    @Test
    @DisplayName("Поиск задачи по Id для пользователя")
    public void findOneByIdForUserTest() throws Exception {
        @Nullable Task task;
        @NotNull List<Task> taskListForUser = taskList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 0; i < taskListForUser.size(); i++) {
            task = taskListForUser.get(i);
            Assert.assertNotNull(task);
            @NotNull final String taskId = task.getId();
            @Nullable final Task foundTask = taskRepository.findOneById(USER_1_ID, taskId);
            Assert.assertNotNull(foundTask);
        }
    }

    @Test
    @DisplayName("Поиск задачи по Id равному Null для пользователя")
    public void findOneByIdNullForUserTest() throws Exception {
        @Nullable final Task foundTask = taskRepository.findOneById(USER_1_ID, "qwerty");
        Assert.assertNull(foundTask);
        @Nullable final Task foundTaskNull = taskRepository.findOneById(USER_1_ID, null);
        Assert.assertNull(foundTaskNull);
    }

    @Test
    @DisplayName("Поиск задачи по индексу")
    public void findOneByIndexTest() throws Exception {
        for (int i = 1; i <= taskList.size(); i++) {
            @Nullable final Task task = taskRepository.findOneByIndex(i);
            Assert.assertNotNull(task);
        }
    }

    @Test
    @DisplayName("Поиск задачи по индексу равному Null")
    public void findOneByIndexNullTest() throws Exception {
        @Nullable final Task task = taskRepository.findOneByIndex(null);
        Assert.assertNull(task);
    }

    @Test
    @DisplayName("Поиск задачи по индексу для пользователя")
    public void findOneByIndexForUserTest() throws Exception {
        @NotNull List<Task> taskListForUser = taskList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 1; i <= taskListForUser.size(); i++) {
            @Nullable final Task task = taskRepository.findOneByIndex(USER_1_ID, i);
            Assert.assertNotNull(task);
        }
    }

    @Test
    @DisplayName("Поиск задачи по индексу равному Null")
    public void findOneByIndexNullForUserText() throws Exception {
        @Nullable final Task task = taskRepository.findOneByIndex(USER_1_ID, null);
        Assert.assertNull(task);
    }

    @Test
    @DisplayName("Получить количество задач")
    public void getSizeTest() throws Exception {
        int actualSize = taskRepository.getSize();
        Assert.assertTrue(actualSize > 0);
    }

    @Test
    @DisplayName("Получить количество задач для пользователя")
    public void getSizeForUserTest() throws Exception {
        int expectedSize = (int) taskList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .count();
        int actualSize = taskRepository.getSize(USER_1_ID);
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    @DisplayName("Удалить пустой список задач")
    public void removeAllNullTest() throws Exception {
        int expectedNumberOfEntries = taskRepository.getSize();
        taskRepository.removeAll(null);
        int actualNumberOfEntries = taskRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    @DisplayName("Удалить задачу")
    public void removeTest() throws Exception {
        @Nullable final Task task = taskList.get(0);
        Assert.assertNotNull(task);
        @NotNull final String taskId = task.getId();
        @Nullable final Task deletedTask = taskRepository.remove(task);
        Assert.assertNotNull(deletedTask);
        @Nullable final Task deletedTaskInRepository = taskRepository.findOneById(taskId);
        Assert.assertNull(deletedTaskInRepository);
    }

    @Test
    @DisplayName("Удалить Null задачу")
    public void removeNullTest() throws Exception {
        @Nullable final Task task = taskRepository.remove(null);
        Assert.assertNull(task);
    }

    @Test
    @DisplayName("Удалить задачу по Id")
    public void removeByIdTest() throws Exception {
        int index = taskList.size();
        while (index > 0) {
            @Nullable final Task task = taskList.get(index - 1);
            Assert.assertNotNull(task);
            @NotNull final String taskId = task.getId();
            @Nullable final Task deletedTask = taskRepository.removeById(taskId);
            Assert.assertNotNull(deletedTask);
            @Nullable final Task deletedTaskInRepository = taskRepository.findOneById(taskId);
            Assert.assertNull(deletedTaskInRepository);
            index--;
        }
    }

    @Test
    @DisplayName("Удалить задачу по Id равному Null")
    public void removeByIdNullTest() throws Exception {
        @Nullable final Task deletedTask = taskRepository.removeById("qwerty");
        Assert.assertNull(deletedTask);
        @Nullable final Task deletedTaskNull = taskRepository.removeById(null);
        Assert.assertNull(deletedTaskNull);
    }

    @Test
    @DisplayName("Удалить задачу по Id для пользователя")
    public void removeByIdForUserTest() throws Exception {
        int index = (int) taskList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final Task task = taskRepository.findOneByIndex(USER_1_ID, index);
            Assert.assertNotNull(task);
            @NotNull final String taskId = task.getId();
            @Nullable final Task deletedTask = taskRepository.removeById(USER_1_ID, taskId);
            Assert.assertNotNull(deletedTask);
            @Nullable final Task deletedTaskInRepository = taskRepository.findOneById(USER_1_ID, taskId);
            Assert.assertNull(deletedTaskInRepository);
            index--;
        }
    }

    @Test
    @DisplayName("Удалить задачу по Id равному Null для пользователя")
    public void removeByIdNullForUserTest() throws Exception {
        @Nullable final Task deletedTask = taskRepository.removeById(USER_1_ID, "qwerty");
        Assert.assertNull(deletedTask);
        @Nullable final Task deletedTaskNull = taskRepository.removeById(USER_1_ID, null);
        Assert.assertNull(deletedTaskNull);
    }

    @Test
    @DisplayName("Удалить задачу по индексу равному Null")
    public void removeByIndexNullTest() throws Exception {
        @Nullable final Task deletedTask = taskRepository.removeByIndex(null);
        Assert.assertNull(deletedTask);
    }

    @Test
    @DisplayName("Удалить задачу по индексу для пользователя")
    public void removeByIndexForUserTest() throws Exception {
        int index = (int) taskList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final Task deletedTask = taskRepository.removeByIndex(USER_1_ID, index);
            Assert.assertNotNull(deletedTask);
            @NotNull final String taskId = deletedTask.getId();
            @Nullable final Task deletedTaskInRepository = taskRepository.findOneById(USER_1_ID, taskId);
            Assert.assertNull(deletedTaskInRepository);
            index--;
        }
    }

    @Test
    @DisplayName("Удалить задачу по индексу равному Null для пользователя")
    public void removeByIndexNullForUserTest() throws Exception {
        @Nullable final Task deletedTask = taskRepository.removeByIndex(USER_1_ID, null);
        Assert.assertNull(deletedTask);
    }

}